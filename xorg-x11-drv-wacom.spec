%global moduledir %(pkg-config xorg-server --variable=moduledir )

Name:           xorg-x11-drv-wacom
Version:        1.2.3
Release:        1
License:        GPL-2.0-or-later
Summary:        Xorg X11 wacom input driver
URL:            https://github.com/linuxwacom/xf86-input-wacom/wiki
Source0:        https://github.com/linuxwacom/xf86-input-wacom/releases/download/xf86-input-wacom-%{version}/xf86-input-wacom-%{version}.tar.bz2
Patch0:         xf86-input-wacom-fix-variable-name-when-dealing-with-systemd-unit-dir.patch
Patch1:         xf86-input-wacom-rename-wacom-rules.patch

BuildRequires: meson >= 0.51.0
BuildRequires: pkgconfig(inputproto)
BuildRequires: pkgconfig(kbproto)
BuildRequires: pkgconfig(libudev)
BuildRequires: pkgconfig(randrproto)
BuildRequires: pkgconfig(x11)
BuildRequires: pkgconfig(xext)
BuildRequires: pkgconfig(xi)
BuildRequires: pkgconfig(xinerama)
BuildRequires: pkgconfig(xorg-server) >= 1.13.0
BuildRequires: pkgconfig(xproto)
BuildRequires: pkgconfig(xrandr)
Requires:       Xorg %(xserver-sdk-abi-requires ansic) Xorg %(xserver-sdk-abi-requires xinput)

Provides:       linuxwacom = %{version}-%{release}
Obsoletes:      linuxwacom < 0.8.4.3
Provides:       %{name}-serial-support = %{version}-%{release}
Obsoletes:      %{name}-serial-support < %{version}-%{release}

%description
X.Org X11 wacom input driver for Wacom tablets.

%package        devel
Summary:        Xorg X11 wacom input driver development package
Requires:       xorg-x11-server-devel >= 1.13.0

%description    devel
X.Org X11 wacom input driver development files.

%package_help

%prep
%autosetup -n xf86-input-wacom-%{version} -p1

%build
# disable unittests due to missing python3-libevdev in everything
%meson -Dunittests=disabled -Dwacom-gobject=disabled
%meson_build

%install
%meson_install

%check
%meson_test

%files
%doc AUTHORS
%license GPL
%{_datadir}/X11/xorg.conf.d/70-wacom.conf
%{_udevrulesdir}/70-wacom.rules
%{_unitdir}/wacom-inputattach@.service
%{_bindir}/isdv4-serial-inputattach
%{_bindir}/xsetwacom
%{moduledir}/input/wacom_drv.so

%files  devel
%{_libdir}/pkgconfig/*.pc
%{_bindir}/isdv4-serial-debugger
%{_includedir}/xorg/*.h

%files  help
%doc ChangeLog
%{_mandir}/man4/wacom.4*
%{_mandir}/man1/xsetwacom.1*

%changelog
* Sun Nov 17 2024 Funda Wang <fundawang@yeah.net> - 1.2.3-1
- update to 1.2.3

* Fri Jul 14 2023 zhangpan <zhangpan103@h-partners.com> - 1.2.0-1
- upgrade to 1.2.0

* Wed Oct 26 2022 wangkerong <wangkerong@h-partners.com> - 1.1.0-1
- upgrade to 1.1.0

* Sat Nov 27 2021 yangcheng <yangcheng87@huawei.com> - 0.40.0-1
- upgrade to 0.40.0

* Sat Jan 30 2021 jinzhimin <jinzhimin2@huawei.com> - 0.39.0-1
- upgrade to 0.39.0

* Fri Oct 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.36.1-6
- Package init
